"""Tugas2PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
#import app_comment.urls as app_comment
import app_forum.urls as app_forum
import app_profil.urls as app_profil
import app_comment.urls as app_comment
import app_homepage.urls as app_homepage
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url="/app_homepage/", permanent="true"), name='app_homepage'),
    url(r'^app_comment/', include(app_comment, namespace='app_comment')),
    url(r'^app_homepage/', include(app_homepage,namespace='app_homepage')),
    url(r'^app_forum/', include(app_forum, namespace='app_forum')),
    url(r'^app_profil/', include(app_profil, namespace='app_profil')),

]
