from django.db import models

# Create your models here.
class Update(models.Model):
    message = models.CharField(max_length=140)
    created_date = models.DateTimeField(auto_now_add=True)
# class Reply(models.Model):
#     message = models.CharField(max_length=140)
#     created_date = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
	comment = models.CharField(max_length=160)
	created_date = models.DateTimeField(auto_now_add=True)
	status = models.ForeignKey(Update, null=True, blank=True)
