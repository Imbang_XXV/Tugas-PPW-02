from django.db import models
from app_forum.models import Message
# Create your models here.
class Reply(models.Model):
    message = models.CharField(max_length=140)
    created_date = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
	comment = models.CharField(max_length=160)
	created_date = models.DateTimeField(auto_now_add=True)
	status = models.ForeignKey(Message, null=True, blank=True)

	def __str__(self):
		return self.comment
