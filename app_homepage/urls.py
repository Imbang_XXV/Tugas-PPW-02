from django.conf.urls import url
from .views import index, closeSession, add_comment

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^closeSession/$', closeSession, name='closeSession'),
    url(r'^add_comment/(?P<pk>[0-9]+)/$', add_comment, name='add_comment'),
    #url(r'^result_post', message_table, name='result_post'),
]
