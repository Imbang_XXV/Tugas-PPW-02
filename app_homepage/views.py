from django.shortcuts import render
from .models import Reply, Comment
from django.http import HttpResponseRedirect, HttpResponse
from .forms import Message_Form
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

def index(request):
	response = {'author': 'Team Papow'}
	html = 'app_homepage.html'
	return render(request, html, response)

def closeSession(request):
	response = {'author': 'Team Papow'}
	html = 'app_homepage.html'
	return render(request, html, response)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

@csrf_exempt
def add_comment(request, pk):
    status = Reply.objects.get(pk=pk)
    form = Comment_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['comment'] = request.POST['comment']
        comment=Comment(comment=response['comment'])
        comment.status = status
        comment.save()
        data = model_to_dict(comment)
        return HttpResponse(data)
    else:
        return HttpResponseRedirect('/app_homepage/')
