window.onload = sharedContent;
function sharedContent() {
  console.log("Success"+IN.User.isAuthorized());
  IN.API.Raw("/companies?format=json&is-company-admin=true").result(
    function (user_data) {
      company_data = user_data.values[0];
      comp_id = company_data.id;
      IN.API.Raw(/companies/+comp_id+":(id,name,logo-url,specialties,website-url,description)?format=json").result(saveSession);
    }
    ).error(error403);
}

function saveSession(data){
  sessionStorage.setItem('company', JSON.stringify(data));
  company_id = JSON.parse(sessionStorage.getItem('company')).id;
  console.log(company_id);
}